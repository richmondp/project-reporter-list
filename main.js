// page elements
let pageLoaderElement = document.querySelector('.page-loader');
let queryResultsContainerElement = document.querySelector('#query-results');
let tableBodyElement = document.querySelector('#table-data');
let queryDetailsElement = document.querySelector('#query-details');
let pageErrorContainerElement = document.querySelector('.page-errors');
let pageAnouncementElement = document.querySelector('#no-results');

/**
 * Initialises properties and loads initial page data. To be called once the page has been loaded.
 */
function initialise() {
	AP.context.getContext((response) => {
		let jiraProjectKey = response.jira.project.key; // get this safely
		fetchReportersForProject(jiraProjectKey);
	});
}

/**
 * Makes requests to the Search API to return and process a list of issues. Once issues have been processed they are
 * added to the page for display.
 *
 * If an error occurs or no results are returned the view is updated to represent each scenario.
 *
 * Current implementation will make 1 request with maxResults=100. If the total number of records is > than 100 then
 * subsequent requests are generated and run in parallel until all results have been returned.
 *
 * @todo: This is an extremely inefficient way to handle this search and it is not scalable as issues increase.
 *     All requests have to be made up font and store client side. Need to look at improving this approach.
 *
 * @todo: Error handling is only handled generically. Need to look at a better approach for more fine grained error handling
 *
 * @param projectKey The unique key for a JIRA Service Desk Project
 */
async function fetchReportersForProject(projectKey) {
	try {
		let data = await AP.request(buildSearchRequest(projectKey, 0, 100));

		let responseBodyAsJSON = JSON.parse(data.body);
		let {startAt, maxResults, total, issues} = responseBodyAsJSON;
		// determine if there are more pages of issues, all issues have to be fetched so our results will be correct
		if (maxResults < total) {
			let additionalCallCount = Math.ceil(total / maxResults) - 1; // minus one for the first call
			let additionalCalls = generateAdditionalCallRequests(additionalCallCount, projectKey, startAt, maxResults);
			let additionalPageData = await Promise.all(additionalCalls);

			// todo: tidy up.
			additionalPageData.forEach(pageData => {
				let jsonData = JSON.parse(pageData.body);
				issues.push(...jsonData.issues);
				handleSearchResponse(convertSearchResponse(issues));
			})
		} else if (total === 0) {
			handleNoResults();
		} else {
			handleSearchResponse(convertSearchResponse(issues));
		}
	} catch (err) {
		handleErrorResponse(err);
	}
}

/**
 *
 * Builds a list (size=additionalCallCount) of Ap.request objects with urls generated using parameters
 *
 * @param additionalCallCount The number of additional Ap.request objects that need to be constructed
 * @param projectKey The unique key of the project the Search request will use
 * @param startAt The start position to the Search API will start returning results from
 * @param maxResults The maximum number of results to be returned by the Search API request
 * @returns {Array} Ap.request promise objects to the Search API with generated url
 */
function generateAdditionalCallRequests(additionalCallCount, projectKey, startAt, maxResults) {
	let additionalCalls = [];
	[...Array(additionalCallCount)].forEach(() => {
		startAt += maxResults;
		additionalCalls.push(AP.request(buildSearchRequest(projectKey, startAt, maxResults)));
	});

	return additionalCalls;
}

/**
 * Constructs a Search API url string using paramaters provided
 *
 * @param projectKey
 * @param startAt
 * @param maxResults
 * @returns {string}
 */
function buildSearchRequest(projectKey, startAt, maxResults) {
	return `/rest/api/3/search?jql=project=${projectKey}&startAt=${startAt}&maxResults=${maxResults}`;
}

/**
 * Updates the view with error details
 *
 * @todo: This is only a basic handling. This needs to be fleshed out further catering for a number of error scenarios
 *
 * @param err The error Object
 */
function handleErrorResponse(err) {
	pageErrorContainerElement.innerHTML = `<div>An error has occurred</div>`;
	toggleClass(pageErrorContainerElement, 'hidden');
	toggleClass(pageLoaderElement, 'hidden');
}

/**
 * Sorts a Map<String, Object> by their # of issues (DESC) and Name (ASC) then displays this list as a table in the view.
 *
 * @param reportersMap
 */
function handleSearchResponse(reportersMap) {
	let sortedReporterMap = sortReportersMap(reportersMap);
	populateResults(sortedReporterMap);

	toggleClass(pageLoaderElement, 'hidden');
	toggleClass(queryResultsContainerElement, 'hidden');
}

/**
 * Displays a message in the view when no results are returned
 *
 * @param reportersMap
 */
function handleNoResults(reportersMap) {

	toggleClass(pageLoaderElement, 'hidden');
	toggleClass(pageAnouncementElement, 'hidden');
}

/**
 *
 * @param element
 * @param className
 */
function toggleClass(element, className) {
	if (element.classList.contains(className)) {
		element.classList.remove(className);
	} else {
		element.classList.add(className)
	}
}

/**
 * Creates a list of Reporters into a distinct Map or reporters associated to their details including total number of issues
 *
 * @param projectIssues An Array containing JSON that represents an Issue
 * @returns {Map<String, Object>} where the String key is the id of the reporter and the Object value contains
 *     {
 *         name,
 *         email,
 *         issues
 *     }
 */
function convertSearchResponse(projectIssues) {
	let reporters = new Map();

	projectIssues.forEach(issue => {
		let reporter = issue.fields.reporter;
		let {displayName: name, emailAddress: email, accountId: reporterId} = reporter;

		if (reporters.has(reporterId)) {
			reporters.get(reporterId).issues.push(issue); // if we are only displaying a count do we need the array?
		} else {
			reporters.set(reporterId, {
				name,
				email,
				issues: [issue]
			})
		}
	});

	return reporters;
}

/**
 * Takes a Map<String, Object> and re-sorts the Map
 *
 * @see compareReporters
 *
 * @param reportersMap
 */
function sortReportersMap(reportersMap) {
	return new Map([...reportersMap].sort(compareReporters));
}

/**
 * Takes a Map of distinct Issue reporters and for each reporter updates the view with a new <tr> containing the
 * reporter name and # of issues raised
 *
 *
 * @param reportersMap: Map<String, Object>
 */
function populateResults(reportersMap) {

	let reporters = [...reportersMap.values()];
	let issueCount = reporters.reduce((accumulator, currentValue) => {
		return accumulator + currentValue.issues.length
	}, 0);

	reportersMap.forEach(reporter => {
		let listItem = `<tr>
                    <td class="sd-customer-org" headers="reporter-name">
                        <div class="sd-cell-contents sd-cell-name">
                            <span class="sd-user-value">${reporter.name}</span>
                            <span class="js-customer-list-email"> (${reporter.email})</span>
                        </div>
                    </td>
                    <td class="sd-closed-requests-count-shown" headers="reporter-issue-count">
                        <div class="sd-cell-contents">${reporter.issues.length}</div>
                    </td>
                </tr>`;

		tableBodyElement.innerHTML = tableBodyElement.innerHTML + listItem;
	});

	queryDetailsElement.innerHTML = `<div>Total number of reporters: ${reporters.length}</div><div>Total number of issues: ${issueCount}</div>`
}

/**
 * Compares two Reporter objects and sorts them based upon # of issues (DESC) and name (ASC)
 *
 * @param firstValue
 * @param secondValue
 * @returns {number}
 */
function compareReporters(firstValue, secondValue) {

	// Descending value compare for # of issues
	let comparison = secondValue[1].issues.length - firstValue[1].issues.length;

	// if # issues are the same run a Ascending compare on reporter name
	if (comparison === 0) {
		comparison = firstValue[1].name.localeCompare(secondValue[1].name);
	}

	return comparison;
}