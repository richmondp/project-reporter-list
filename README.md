# Top Reporters Add-On

An MVP to provide a list of top reporters for a JIRA Service Desk project. 

## Outstanding issues

### Data retrieval

The data retrieval needs to be looked at. Due to the current data structure and existing API functionality there 
no way to aggregate the data and retrieve it in one call or via a series of paginated calls. The implementation 
I have taken here is an inefficient hack to get the data displayed on the page. To handle a problem like this outside
of JIRA/JQL I would have preferred to group the data the data at the back-end and return a series of paginated results.
That said their might be a more appropriate way to return the results with the current data structure but I was unable 
to find a solution. 

### Error Handling

Current error handling is only generic and will display the same message for any error. Work would have to be done to 
add some additional logic to inspect the returned error object and display an appropriate message.

### Testing

I made the decision given the brief said this was a very rough proof of concept that I would not included tests. This 
is obviously something I would not do if building a more robust. I would normally include something like jasmine but if
using react I would probably include Jest. 

### Build

Since I was building a prototype I decided not to implement a build process (build scripts, bundling etc). Moving forward
I would look at including some npm scripts to run the build/tests etc. I would also include a webpack configuration
to include things like minification, gziping, linting etc. 

### Layout 

Given the current simple nature of the application I didn't introduce any details structural layout to the project. This
needs to be addressed. 

### Layout/styles

I decided that for a rough proof of concept that styles and layout were not important. Going forward there would need 
to be a bit of work done in this area. Given this is a plug-in into JIRA Service Desk it would be important to 
keep styling similar. Use of the Atlas Kit style bundle would be preferred. I went some way to try an include a table
that follows existing guide lines (shamelessly ripped from an existing table in JIRA) but work still needs to be done 
on this table and the text around it. There is also some jumping of elements on the screen when things are added/removed
from the DOM which need to be addressed. 

Correct icons would also need to be added to the menu item. 

### Types

I am making some pretty big assumptions on data structures and properties being available throughout the code and 
this didn't cause too many problems on this small MVP but going forward I would want to introduce some type 
checking such as TypeScript or Flow. 

### Accessibilty 

No work was done to make this web accessible. Some work would have to be done to ensure this meets web accessibility 
standards. 

### Libraries

I've written this in plain JS but moving forward I would look at introducing some libraries such as React to bring 
in some convinces and also to potentially prevent me from having to reinvent the wheel writing some common utilities. 


### Menu

I noticed late that the **Top reporters** menu was not nested in an **Add-ons** menu as outlined in the 
_JSD remote task.pdf_. I tried to implement menu nesting but was unable to get it to work.
